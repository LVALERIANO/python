milista = ["Juan", "Luis", "Brenda", "Vanessa"]

print(milista)
print(len)

milista.append("Diana")
print(milista)

milista.insert(1,"Roxana")
print(milista)

milista.extend(["Oslo", "Roberto"])
print(milista)

milista.remove("Diana")
print(milista)

milista.pop()
print(milista)

print(milista.index("Brenda"))
print("Fernanda" in milista)
print("Vanessa" in milista)

print(len(milista))